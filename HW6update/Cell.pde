abstract class Cell
{
  protected int row, col;
  protected color cellColor;
  protected final color ALIVE;
  protected final color DEAD;
  protected final color SICK;
  
  
  Cell(int locX, int locY)
  {
    row = locX;
    col = locY;
    ALIVE = color(0, 200, 0);
    DEAD = color(0);
    SICK = color(200, 0, 0); 
  }
  
  abstract void setColor();
  protected color getCellColor() { return cellColor; }
  protected int getCellRow() { return row; }
  protected int getCellCol() { return col; }
  protected void setCellRow(int r) { row = r; }
  protected void setCellCol(int c) { col = c; }
}

class LiveCell extends Cell
{
  LiveCell(int locX, int locY)
  {
    super(locX, locY);
    setColor();
  }
  
  @Override
  public void setColor() {
    cellColor = ALIVE;
  }
}

class DeadCell extends Cell
{
  DeadCell (int locX, int locY)
  {
    super(locX, locY);
    setColor();
  }
  
  @Override
  public void setColor() {
    cellColor = DEAD;
  }
}

class SickCell extends Cell 
{
  SickCell (int locX, int locY)
  {
    super(locX, locY);
    setColor();
  }
  
  @Override
  public void setColor() {
    cellColor = SICK;
  }
}
