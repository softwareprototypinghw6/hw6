//////////////////////
import android.view.MotionEvent;

import ketai.ui.*;
import ketai.sensors.*;



KetaiGesture gesture;
KetaiSensor sensor;

int TouchEvents;

float accelerometerX, accelerometerY, accelerometerZ;
float lastX, lastY, lastZ;
final int SHAKE_THRESHOLD = 2000;

long lastTime;
float speed;

float xTouch, yTouch;
/////////////////////

Board b;
PFont font;

void setup()
{
  
  /////////////////////
  orientation(PORTRAIT);
  gesture = new KetaiGesture(this);
  sensor = new KetaiSensor(this);
  sensor.start();
  fullScreen();
  ////////////////////
  
  //size(640, 640);
  b = new Board(width,height);
  b.initializeBoard();
  font = createFont("LetterGothic.ttf", 24);
  
}

void draw()
{
  background(0);
  b.draw();
}


///////////////////////
void onPinch(float x, float y, float d)
{
  //TODO
  print("pinch : "+d);
}

void allTap()
{
  //TODO
  print("allTap");
}

void shake()
{
  //TODO
  print("shake");
}

void touch(float xTouch, float yTouch)
{
  //TODO
  print("touch : "+ xTouch +", "+yTouch);
}


void onAccelerometerEvent(float x, float y, float z)
{  
  long currentTime = millis();
  long gabOfTime = (currentTime - lastTime);
  if (gabOfTime > 100) {
      lastTime = currentTime;
      accelerometerX = x;
      accelerometerY = y;
      accelerometerZ = z;

      speed =
          Math.abs(x + y + z - lastX - lastY - lastZ)
          / gabOfTime * 10000;//can edit but not necessary

      if (speed > SHAKE_THRESHOLD) {
          shake();
      }

      lastX = x;
      lastY = y;
      lastZ = z;
  }

}

public boolean surfaceTouchEvent(MotionEvent event) {
  super.surfaceTouchEvent(event);
  TouchEvents = event.getPointerCount();
  
  if (event.getActionMasked() == 5) {
    if(TouchEvents == 4) allTap();
  }
  
  else { 
    xTouch = event.getX(0); 
    yTouch = event.getY(0);
    touch(xTouch, yTouch);
  }
  return gesture.surfaceTouchEvent(event);
}
//////////////////////////////////////