package com.makinteract.androidprocessing;

import android.util.Log;
import android.view.MotionEvent;

import java.util.ArrayList;

import ketai.sensors.KetaiSensor;
import ketai.ui.KetaiGesture;

import processing.core.*;


public class Sketch extends PApplet {


    KetaiGesture gesture;
    KetaiSensor sensor;

    int touchEvents;

    float accelerometerX, accelerometerY, accelerometerZ;
    float lastX, lastY, lastZ;
    final int SHAKE_THRESHOLD = 2000;

    long lastTime;
    float speed;

    float xTouch, yTouch;
/////////////////////

    Board b;
    PFont font;

    public void settings() {
        fullScreen();
    }

    public void setup()
    {

        /////////////////////
        orientation(PORTRAIT);
        gesture = new KetaiGesture(this);
        sensor = new KetaiSensor(this);
        sensor.start();
        fullScreen();
        ////////////////////

        //size(640, 640);
        b = new Board(width,height);
        b.initializeBoard();
        font = createFont("LetterGothic.ttf", 120);
        textSize(50);
    }

    public void draw()
    {
        background(0);
        b.draw();
    }


    ///////////////////////
    void onPinch(float x, float y, float d)
    {
        if(b.isEditMode() && Math.abs(d)>10) {
            b.initializeBoard();
            b.changeCellSize((int) d);
        }
    }

    void allTap()
    {
        //TODO
        if(b.isEditMode()){
            b.day = 0;
        }
        if(b.finished) {b.finished = false; b.initializeBoard();}
        b.changeEditMode();
    }

    void shake()
    {
        //TODO
        Log.d("gesture","shake");
        if(b.isEditMode()) b.initializeBoard();
    }

    void touch(float xTouch, float yTouch)
    {
        //TODO
      if(b.isEditMode())
       {
           b.edit(xTouch, yTouch);
       }

    }


    void onAccelerometerEvent(float x, float y, float z)
    {
        long currentTime = millis();
        long gabOfTime = (currentTime - lastTime);
        if (gabOfTime > 100) {
            lastTime = currentTime;
            accelerometerX = x;
            accelerometerY = y;
            accelerometerZ = z;

            speed =
                    Math.abs(x + y + z - lastX - lastY - lastZ)
                            / gabOfTime * 10000;//can edit but not necessary

            if (speed > SHAKE_THRESHOLD) {
                shake();
            }

            lastX = x;
            lastY = y;
            lastZ = z;
        }

    }

    public boolean surfaceTouchEvent(MotionEvent event) {
        super.surfaceTouchEvent(event);
        touchEvents = event.getPointerCount();

        if (event.getActionMasked() == MotionEvent.ACTION_POINTER_DOWN) {
            if(touchEvents == 5) allTap();
        }
//        else if (event.getActionMasked() == 1){
//            xTouch = event.getX(0);
//            yTouch = event.getY(0);
//            b.tap(xTouch, yTouch);
//        }
        else{
            xTouch = event.getX(0);
            yTouch = event.getY(0);
            touch(xTouch, yTouch);
        }

//        for(Cell c: b.cells) c.touched = false;
        return gesture.surfaceTouchEvent(event);
    }
//////////////////////////////////////


    class Board
    {
        private int CELLSIZE = 50;
        private int val = 50;
        private final int boardWidth;
        private final int boardHeight;

        ArrayList<Cell> cells;
        ArrayList<Cell> tempCells;
        Object lock; // Lock for array
        private final int REFRESH = 10;
        private long prevMillis;
        private int day;
        private boolean editMode;
        private boolean finished;
        private final float probabilityOfAliveAtStart = 15;
        private final float DISEASE = 10;




        Board(int w, int h)
        {
            boardWidth = w;
            boardHeight = h;
            // Instantiate arrays
            cells = new ArrayList<Cell>();
            tempCells = new ArrayList<Cell>();
            prevMillis = 0;
            editMode = true;
            finished = false;
            day = 0;

            lock = new Object();
        }

        // REPLACE WITH TOUCH ACTION
        void initializeBoard() {
            synchronized (lock) {
                cells.clear();
                tempCells.clear();

                for (int x = 0; x < boardWidth / CELLSIZE; x++) {
                    for (int y = 0; y < boardHeight / CELLSIZE; y++) {
//                    float state = random(100);
//                    if (state > probabilityOfAliveAtStart)
//                    {
                        cells.add(new DeadCell(x, y));
                        tempCells.add(new DeadCell(x, y));
// }
//                    else
//                    {
//                        cells.add(new LiveCell(x,y));
//                    }
                    }
                }
            }
        }

//        void cleared()
//        {
//            cells.clear();
//            for (int x = 0; x < boardWidth/CELLSIZE; x++)
//            {
//                for (int y = 0; y < boardHeight/CELLSIZE;y++)
//                {
//                        cells.add(new DeadCell(x,y));
//                }
//            }
//        }

//        void edit(float x, float y) {
//            //do Edit instead of initiallizeBoard up there.
//            xCellOver = (int) map(x, 0, width, 0, width / CELLSIZE);
//            xCellOver = constrain(xCellOver, 0, width / CELLSIZE - 1);
//            yCellOver = (int) map(y, 0, height, 0, height / CELLSIZE);
//            yCellOver = constrain(yCellOver, 0, height / CELLSIZE - 1);
//
//            tempCells.get(getCellIndex(xCellOver, yCellOver)).touched = true;
//
//            for (Cell c : tempCells) {
//                if (c.touched == false && c.lasttouched == true) {
//                    if (c instanceof LiveCell || c instanceof SickCell) {
//                        cells.set(getCellIndex(c.row, c.col), new DeadCell(c.row, c.col));
//                    }
//                    else {
//                        cells.set(getCellIndex(c.row, c.col), new LiveCell(c.row, c.col));
//                    }
//                }
//            }
//
//            for(Cell c : tempCells) c.lasttouched = c.touched;
////            if(xCellOver != lastxCellOver || yCellOver != lastyCellOver) {
////                // Check against cells in buffer
////                if (tempCells.get(getCellIndex(lastxCellOver, lastyCellOver)) instanceof LiveCell ||
////                        tempCells.get(getCellIndex(lastxCellOver, lastyCellOver)) instanceof SickCell) { // Cell is alive
////                    cells.set(getCellIndex(lastxCellOver, lastyCellOver), new DeadCell(lastxCellOver, lastyCellOver)); // Kil
////                } else { // Cell is dead
////                    cells.set(getCellIndex(lastxCellOver, lastyCellOver), new LiveCell(lastxCellOver, lastyCellOver)); // Make alive
////                }
////            }
////            lastxCellOver = xCellOver;
////            lastyCellOver = yCellOver;
//        }

        void edit(float x, float y)
        {
            //do Edit instead of initiallizeBoard up there.
            int xCellOver = (int) map(x, 0, width, 0, width/CELLSIZE);
            xCellOver = constrain(xCellOver, 0, width/CELLSIZE-1);
            int yCellOver = (int) map(y, 0, height, 0, height/CELLSIZE);
            yCellOver = constrain(yCellOver, 0, height/CELLSIZE-1);

            // Check against cells in buffer
            if (tempCells.get(getCellIndex(xCellOver, yCellOver)) instanceof LiveCell ||
                    tempCells.get(getCellIndex(xCellOver, yCellOver)) instanceof SickCell) { // Cell is alive
                cells.set(getCellIndex(xCellOver, yCellOver), new DeadCell(xCellOver, yCellOver)); // Kil
            } else { // Cell is dead
                cells.set(getCellIndex(xCellOver, yCellOver), new LiveCell(xCellOver, yCellOver)); // Make alive
            }
        }



        boolean isEditMode()
        {
            return editMode;
        }
        void changeEditMode()
        {
            editMode = !editMode;
        }

        void changeCellSize(int d)
        {
//            cells = new ArrayList<Cell>();
            //see the value of "d" and may be we should change the value.
            val = constrain(val+d/3,0, 70);
            Log.d("value", Integer.toString(val));

           if(val <= 25) {CELLSIZE = 30;}
            else if(val <= 45) {CELLSIZE = 40;}
            else if (val <= 55) {CELLSIZE = 50;}
            else if(val  <= 65) {CELLSIZE = 60;}
            else {CELLSIZE = 70;}

        }

        void draw()
        {
            synchronized (lock) {
                for (Cell c : cells) {
                    if (!editMode) noStroke();
                    else stroke(100);
                    fill(c.getCellColor());
                    rect(c.getCellRow() * CELLSIZE, c.getCellCol() * CELLSIZE, CELLSIZE, CELLSIZE);
                }

                // Iterate if timer ticks
                if (millis() - prevMillis > REFRESH) {
                    if (!editMode && !finished) {
                        update();
                        prevMillis = millis();
                        day++;
                    }
                    if (!editMode && finished) {
                        fill(255);
                        text("The Cells lasted for " + day + " days", 20, height / 2);

                    }
                }

                if (editMode) {
                    for (Cell c : cells) tempCells.set(getCellIndex(c.row, c.col), c);
                }
                if (!editMode) displayDay();
            }
        }

        void update() {
            finished = true;
            for (Cell c : cells) {
                if (c instanceof LiveCell) finished = false;
            }

            if (tempCells!=null) tempCells.clear();
            for (Cell c : cells) tempCells.add(c);

            // refered to https://processing.org/examples/gameoflife.html
            for (int x=0; x<boardWidth/CELLSIZE; x++) {
                for (int y=0; y<boardHeight/CELLSIZE; y++) {
                    float neighbours = 0;
                    for (int xx=x-1; xx<=x+1;xx++) {
                        for (int yy=y-1; yy<=y+1;yy++) {
                            if (((xx>=0)&&(xx<boardWidth/CELLSIZE))&&((yy>=0)&&(yy<boardHeight/CELLSIZE))) {
                                if (!((xx==x)&&(yy==y))) {
                                    if (tempCells.get(getCellIndex(xx,yy)) instanceof LiveCell) neighbours ++;
                                    else if (tempCells.get(getCellIndex(xx, yy)) instanceof SickCell) neighbours += 1/2;
                                }
                            }
                        }
                    }

                    if (tempCells.get(getCellIndex(x,y)) instanceof LiveCell) {
                        if (neighbours < 2 || neighbours > 3) {
                            cells.set(getCellIndex(x,y), new DeadCell(x,y));
                        }
                    }
                    else if (tempCells.get(getCellIndex(x,y)) instanceof DeadCell) {
                        if (neighbours == 3) {
                            cells.set(getCellIndex(x,y), new LiveCell(x,y));
                        }
                    }
                }
            }

            // Sick Cell appears
            if (day != 0 && day % 10 == 0) {
                for (int x=0; x<boardWidth/CELLSIZE; x++) {
                    for (int y=0; y<boardHeight/CELLSIZE; y++) {
                        int sickRate = (int)random(100);
                        if (sickRate < DISEASE) cells.set(getCellIndex(x, y), new SickCell(x, y));
                    }
                }
            }
        }

        private void displayDay() {
            textFont(font);
            fill(255);
            text("Day : " + day, 20, height - 20);
        }

        int getCellIndex(int x, int y)
        {
            for (int i = 0; i < cells.size(); i++)
            {
                if (cells.get(i).getCellRow() == x && cells.get(i).getCellCol() == y)
                    return i;
            }
            return 0;
        }
    }

    abstract class Cell
    {
        protected int row, col;
        protected int cellColor;
        protected final int ALIVE;
        protected final int DEAD;
        protected final int SICK;

//        protected boolean touched;
//        protected boolean lasttouched;

        Cell(int locX, int locY)
        {
            row = locX;
            col = locY;
            ALIVE = color(0, 200, 0);
            DEAD = color(0);
            SICK = color(200, 0, 0);
//            touched = false;
//            lasttouched = false;
        }

        abstract void setColor();
        protected int getCellColor() { return cellColor; }
        protected int getCellRow() { return row; }
        protected int getCellCol() { return col; }
        protected void setCellRow(int r) { row = r; }
        protected void setCellCol(int c) { col = c; }
    }

    class LiveCell extends Cell
    {
        LiveCell(int locX, int locY)
        {
            super(locX, locY);
            setColor();
        }

        @Override
        public void setColor() {
            cellColor = ALIVE;
        }
    }

    class DeadCell extends Cell
    {
        DeadCell (int locX, int locY)
        {
            super(locX, locY);
            setColor();
        }

        @Override
        public void setColor() {
            cellColor = DEAD;
        }
    }

    class SickCell extends Cell
    {
        SickCell (int locX, int locY)
        {
            super(locX, locY);
            setColor();
        }

        @Override
        public void setColor() {
            cellColor = SICK;
        }
    }

}


