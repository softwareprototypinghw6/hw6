class Board
{
  private final int CELLSIZE = 20;
  private int boardWidth;
  private int boardHeight;
  
  ArrayList<Cell> cells;
  ArrayList<Cell> tempCells;
  private final int REFRESH = 100;
  private long prevMillis;
  private int day;
  private boolean editMode;
  private boolean finished;
  private final float probabilityOfAliveAtStart = 15;
  private final float DISEASE = 10;
  
  
  Board(int w, int h)
  {
    boardWidth = w;
    boardHeight = h;
    // Instantiate arrays 
    cells = new ArrayList<Cell>();
    tempCells = new ArrayList<Cell>();
    prevMillis = 0;
    editMode = false;
    finished = false;
    day = 0;
  }

// REPLACE WITH TOUCH ACTION
  void initializeBoard()
  {
    for (int x = 0; x < boardWidth/CELLSIZE; x++)
    {
      for (int y = 0; y < boardHeight/CELLSIZE;y++)
      {
        float state = random(100);
        if (state > probabilityOfAliveAtStart)
        {
          cells.add(new DeadCell(x,y));
        }
        else
        {
          cells.add(new LiveCell(x,y));
        }
      }
    }
  }
  
  void draw()
  {
    for (Cell c : cells)
    {
      fill(c.getCellColor());
      rect(c.getCellRow()*CELLSIZE, c.getCellCol()*CELLSIZE, CELLSIZE, CELLSIZE);
    }
    
    // Iterate if timer ticks
    if (millis()-prevMillis>REFRESH) {
      if (!editMode && !finished) {
        update();
        prevMillis = millis();
        day++;
      }
      if (!editMode && finished) {
        fill(255);
        text("The Cells lasted for " + day + " days", 20, height/2);
      }
    }
    displayDay();
  }
  
  void update() {
    finished = true;
    for (Cell c : cells) {
      if (c instanceof LiveCell) finished = false;
    } 
    
    if (tempCells!=null) tempCells.clear();
    for (Cell c : cells) tempCells.add(c);
  
    // refered to https://processing.org/examples/gameoflife.html
    for (int x=0; x<boardWidth/CELLSIZE; x++) {
      for (int y=0; y<boardHeight/CELLSIZE; y++) {
        float neighbours = 0;
        for (int xx=x-1; xx<=x+1;xx++) {
          for (int yy=y-1; yy<=y+1;yy++) {  
            if (((xx>=0)&&(xx<boardWidth/CELLSIZE))&&((yy>=0)&&(yy<boardHeight/CELLSIZE))) {
              if (!((xx==x)&&(yy==y))) {
                if (tempCells.get(getCellIndex(xx,yy)) instanceof LiveCell) neighbours ++;
                else if (tempCells.get(getCellIndex(xx, yy)) instanceof SickCell) neighbours += 1/2;
              }
            }
          }
        }

        if (tempCells.get(getCellIndex(x,y)) instanceof LiveCell) {
          if (neighbours < 2 || neighbours > 3) {
            cells.set(getCellIndex(x,y), new DeadCell(x,y));
          }
        } 
        else if (tempCells.get(getCellIndex(x,y)) instanceof DeadCell) {
          if (neighbours == 3) {
            cells.set(getCellIndex(x,y), new LiveCell(x,y));
          }
        }
      }
    }
    
    // Sick Cell appears
    if (day != 0 && day % 10 == 0) {
      for (int x=0; x<boardWidth/CELLSIZE; x++) {
        for (int y=0; y<boardHeight/CELLSIZE; y++) {
          int sickRate = (int)random(100);
          if (sickRate < DISEASE) cells.set(getCellIndex(x, y), new SickCell(x, y));
        }
      }
    }
  }
  
  private void displayDay() {
    textFont(font);
    fill(255);
    text("Day : " + day, 20, height - 20);
  }
  
  int getCellIndex(int x, int y)
  {
    for (int i = 0; i < cells.size(); i++)
    {
      if (cells.get(i).getCellRow() == x && cells.get(i).getCellCol() == y)
        return i;
    }
    return 0;
  }
}