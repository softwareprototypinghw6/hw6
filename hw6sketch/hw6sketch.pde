Board b;
PFont font;

void setup()
{
  size(640,360);
  //fullScreen();
  b = new Board(width,height);
  b.initializeBoard();
  font = createFont("LetterGothic.ttf", 24);
  
}

void draw()
{
  background(0);
  b.draw();
}