//code for multi-touch gesture

//-----------------------------------------------------------------------------------------
// IMPORTS

import android.view.MotionEvent;


//-----------------------------------------------------------------------------------------
// VARIABLES

int TouchEvents;
float xTouch[];
float yTouch[];
int currentPointerId = 0;
boolean printFPS;


//-----------------------------------------------------------------------------------------

void setup() {
  size(displayWidth, displayHeight);
  orientation(PORTRAIT);
  background(0, 255, 0);
  fill(0, 0, 244);
  rect(100, 100, 100, 100);
  stroke(255);

  // Initialize Multitouch x y arrays
  xTouch = new float [10];
  yTouch = new float [10]; // Don't use more than ten fingers!

}

//-----------------------------------------------------------------------------------------

void draw() {
  background(255, 0, 0);

  for (int i = 0; i < xTouch.length; i++) {
    ellipse(xTouch[i], yTouch[i], 150, 150);
  }

}

//-----------------------------------------------------------------------------------------

public boolean surfaceTouchEvent(MotionEvent event) {

  // Number of places on the screen being touched:
  TouchEvents = event.getPointerCount();

  // If no action is happening, listen for new events else 
  for (int i = 0; i < TouchEvents; i++) {
    int pointerId = event.getPointerId(i);
    xTouch[pointerId] = event.getX(i); 
    yTouch[pointerId] = event.getY(i);
    float siz = event.getSize(i);
  }

  // ACTION_DOWN 
  if (event.getActionMasked() == 0 ) {
    print("Initial action detected. (ACTION_DOWN)");
    print("Action index: " +str(event.getActionIndex()));
  } 
  // ACTION_UP 
  else if (event.getActionMasked() == 1) {
    print("ACTION_UP");
    print("Action index: " +str(event.getActionIndex()));
  }
  //  ACTION_POINTER_DOWN 
  else if (event.getActionMasked() == 5) {
    print("Secondary pointer detected: ACTION_POINTER_DOWN");
    print("Action index: " +str(event.getActionIndex()));
  }
  // ACTION_POINTER_UP 
  else if (event.getActionMasked() == 6) {
    print("ACTION_POINTER_UP");
    print("Action index: " +str(event.getActionIndex()));
  }
  // 
  else if (event.getActionMasked() == 4) {

  }

  // If you want the variables for motionX/motionY, mouseX/mouseY etc.
  // to work properly, you'll need to call super.surfaceTouchEvent().
  return super.surfaceTouchEvent(event);
}
