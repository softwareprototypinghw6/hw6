import ketai.sensors.*;

KetaiSensor sensor;
float accelerometerX, accelerometerY, accelerometerZ;
float lastX, lastY, lastZ;
final int SHAKE_THRESHOLD = 2000;

long lastTime;
float speed;

void setup()
{
  sensor = new KetaiSensor(this);
  sensor.start();
  orientation(LANDSCAPE);
  textAlign(CENTER, CENTER);
  textSize(36);
}

void draw()
{
  background(78, 93, 75);
  text("Accelerometer: \n" + 
    "x: " + nfp(accelerometerX, 1, 3) + "\n" +
    "y: " + nfp(accelerometerY, 1, 3) + "\n" +
    "z: " + nfp(accelerometerZ, 1, 3), 0, 0, width, height);
}

void onAccelerometerEvent(float x, float y, float z)
{

  
  long currentTime = System.currentTimeMillis();
  long gabOfTime = (currentTime - lastTime);
  if (gabOfTime > 100) {
      lastTime = currentTime;
      accelerometerX = x;
      accelerometerY = y;
      accelerometerZ = z;

      speed =
          Math.abs(x + y + z - lastX - lastY - lastZ)
          / gabOfTime * 10000;//can edit but not necessary

      if (speed > SHAKE_THRESHOLD) {
          //TODO
          print("shake!");
      }

      lastX = x;
      lastY = y;
      lastZ = z;
  }

}